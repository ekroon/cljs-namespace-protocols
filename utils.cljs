(ns utils
  (:require-macros [utils]))

(defprotocol Lifted
  (lifted [this]
          "Returns the object that was lifted using lift-on."))
